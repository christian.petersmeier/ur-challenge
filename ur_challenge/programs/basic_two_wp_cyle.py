"""Contains a program that cycles between two waypoints."""

from ur_challenge.programs.program import Program
from ur_challenge.robots.robot import Robot
from ur_challenge.types.types import ROBOT_OUTPUT_DATA
from ur_challenge.util.conversions import list_to_setp, setp_to_list

import numpy as np


class BasicTwoWPCycle(Program):
    """The program cycles through two waypoints by sending alternating  move commands to the robot."""

    def __init__(self, robot: Robot, name="BasicTwoWPCycle") -> None:
        super().__init__(name)

        self.robot = robot

        # Setpoints to move the robot to
        self.setp1 = [-0.12, -0.43, 0.14, 0, 3.11, 0.04]
        self.setp2 = [-0.12, -0.51, 0.21, 0, 3.11, 0.04]

        # setup recipes
        self.robot.setp.input_double_register_0 = 0
        self.robot.setp.input_double_register_1 = 0
        self.robot.setp.input_double_register_2 = 0
        self.robot.setp.input_double_register_3 = 0
        self.robot.setp.input_double_register_4 = 0
        self.robot.setp.input_double_register_5 = 0

        self.robot.watchdog.input_int_register_0 = 0

        self.move_completed = True

    def enter(self):
        pass

    def step(self):
        # get the current state data from the robot
        data = self.robot.get_data(np.array([ROBOT_OUTPUT_DATA.output_int_register_0]))

        if self.move_completed and data["output_int_register_0"] == 1:
            self.move_completed = False
            new_setp = (
                self.setp1
                if setp_to_list(self.robot.setp) == self.setp2
                else self.setp2
            )
            list_to_setp(self.robot.setp, new_setp)
            print("New pose = " + str(new_setp))
            # send new setpoint
            self.robot.con.send(self.robot.setp)
            self.robot.watchdog.input_int_register_0 = 1
        elif not self.move_completed and data["output_int_register_0"] == 0:
            # print("Move to confirmed pose = " + str(data['target_q']))
            self.move_completed = True
            self.robot.watchdog.input_int_register_0 = 0

        # update the watchdog
        # robot program terminates if watchedog is not updated with f > 1 Hz
        self.robot.con.send(self.robot.watchdog)

    def exit(self):
        pass
