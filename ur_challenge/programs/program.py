"""Defines a generic program."""

from abc import ABC


class Program(ABC):
    """A generic program abstract class."""

    def __init__(self, name="") -> None:
        self.name = name

    def enter(self):
        raise NotImplementedError()

    def step(self):
        raise NotImplementedError()

    def exit(self):
        raise NotImplementedError()
