"""Contains a program that cycles between two waypoints."""

from ur_challenge.programs.program import Program
from ur_challenge.robots.robot import Robot
from ur_challenge.util.conversions import list_to_setp

import numpy as np


class BasicTwoWPCycleConstStream(Program):
    """The program cycles through two waypoints by sending alternating  move commands to the robot."""

    def __init__(self, robot: Robot, name="BasicTwoWPCycleConstStream") -> None:
        super().__init__(name)

        self.robot = robot

        # Setpoints to move the robot to
        self.setp1 = np.round(
            np.deg2rad(np.array([0.0, -43.0, 20.0, 0.0, 40.0, 0.0])), 2
        )
        self.setp2 = np.round(
            np.deg2rad(np.array([-4.0, -54.0, 46.0, -5.0, 35.0, 2.0])), 2
        )

        # setup recipes
        self.robot.setp.input_double_register_0 = 0
        self.robot.setp.input_double_register_1 = 0
        self.robot.setp.input_double_register_2 = 0
        self.robot.setp.input_double_register_3 = 0
        self.robot.setp.input_double_register_4 = 0
        self.robot.setp.input_double_register_5 = 0
        self.robot.watchdog.input_int_register_0 = 0

    def update_current_setp(self):
        self.current_q = np.round(self.robot.get_current_joint_angles(), 2)

        print("q ", self.current_q, " | d ", self.desired_q)

        if (self.current_q == self.desired_q).all():
            # robot has reached position, update desired_q
            if (self.desired_q == self.setp1).all():
                print("target: setp2")
                self.desired_q = self.setp2
            else:
                print("target: setp1")
                self.desired_q = self.setp1

    def enter(self):
        """Execute on entry.

        .. note::
            Needs to be called manually.

        """
        print("target: setp1")
        self.desired_q = self.setp1

    def step(self):
        """Step the program."""

        self.update_current_setp()

        # send current setp
        list_to_setp(self.robot.setp, self.desired_q)
        self.robot.con.send(self.robot.setp)

        # update the watchdog
        # robot program terminates if watchedog is not updated with f > 1 Hz
        self.robot.con.send(self.robot.watchdog)

    def exit(self):
        """Execute on exit.

        .. note::
            Needs to be called manually.

        """
        self.robot.disconnect()
