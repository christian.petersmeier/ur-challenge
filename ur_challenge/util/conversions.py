"""Contains type conversion routines."""


def setp_to_list(sp):
    """Convert a setpoint to a list."""

    sp_list = []
    for i in range(0, 6):
        sp_list.append(sp.__dict__["input_double_register_%i" % i])
    return sp_list


def list_to_setp(sp, list):
    """Convert a list to a setupoint."""

    for i in range(0, 6):
        sp.__dict__["input_double_register_%i" % i] = list[i]
    return sp
