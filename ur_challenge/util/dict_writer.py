"""Returns output data from the robot in numpy format."""

import csv
import sys
from rtde import serialize
import numpy as np


class DictWriter(object):
    def __init__(self, names, types):
        # sanity check
        if len(names) != len(types):
            raise ValueError("List sizes are not identical.")

        # initialize
        self.__names = names
        self.__types = types
        self.__header_names = []
        self.__columns = 0

        # check for variable type size
        for i in range(len(self.__names)):
            size = serialize.get_item_size(self.__types[i])
            self.__columns += size
            if size > 1:
                for j in range(size):
                    name = self.__names[i] + "_" + str(j)
                    self.__header_names.append(name)
            else:
                name = self.__names[i]
                self.__header_names.append(name)

        # initialize writer obejct
        # self.__writer = csv.writer(csvfile, delimiter=delimiter)

        self._outdata = dict()

    @property
    def header_names(self):
        return self.__header_names

    @property
    def outdata(self) -> dict:
        return self._outdata

    def writeheader(self) -> np.ndarray:
        for idx, i in enumerate(self.__header_names):
            self._outdata[i] = np.array([])

    def writerow(self, data_object) -> np.ndarray:
        # initialize outdata
        data = []

        # go through all the variable names
        for i in range(len(self.__names)):
            # get type size and value of the current object
            size = serialize.get_item_size(self.__types[i])
            value = data_object.__dict__[self.__names[i]]

            if size > 1:
                data.extend(value)
            else:
                data.append(value)

        # append new values to the existing values in outdata dict
        for idx, i in enumerate(self.__header_names):
            self._outdata[i] = np.append(self._outdata[i], data[idx])
