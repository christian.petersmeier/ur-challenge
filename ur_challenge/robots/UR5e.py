"""Defines an API for connecting with a UR5e robot via the RTDE protocol."""

import rtde
import numpy as np
from ur_challenge.types import types
from ur_challenge.robots.robot import Robot
from ur_challenge.util.dict_writer import DictWriter
from ur_challenge.types.types import ROBOT_OUTPUT_DATA
import rtde_config
import os


class UR5e(Robot):
    """The UR5e Robot."""

    def __init__(
        self, ip_address: str, port: int, frequency: int = 125, name: str = "UR5e"
    ) -> None:
        super().__init__(ip_address, port, frequency, name)

        # initialize the communication names and types for robot -> pc comm
        self._output_names = None
        self._output_types = None
        self._setp_names = None
        self._setp_types = None
        self._watchdog_names = None
        self._watchdog_types = None

        # initialize the rtde interface
        self._configure_communication(
            config_file=os.path.abspath("./../config/comm_configuration.xml"),
            frequency=frequency,
        )

    def _configure_communication(self, config_file: str, frequency: int = 125):
        """Configure and initialize the communication with the robot.

        Args:
            config (str): The RTDE configuration
            frequency (int): The frequency with which to communicate with the robot.

        Raises:
            RuntimeError: If the initialization of the communication has failed.
        """

        config = rtde_config.ConfigFile(config_file)
        self._output_names, self._output_types = config.get_recipe("state")
        self._setp_names, self._setp_types = config.get_recipe("setp")
        self._watchdog_names, self._watchdog_types = config.get_recipe("watchdog")

        # setup output recipes (robot -> pc)
        if not self._con.send_output_setup(
            self._output_names, self._output_types, frequency=frequency
        ):
            RuntimeError("Unable to configure output!")

        # setup input recipes (pc -> robot)
        self._setp = self._con.send_input_setup(self._setp_names, self._setp_types)
        self._watchdog = self._con.send_input_setup(
            self._watchdog_names, self._watchdog_types
        )

        # start data synchronization
        if not self._con.send_start():
            RuntimeError("Unable to start synchronization")

    @property
    def setp(self):
        return self._setp

    @setp.setter
    def setp(self, var):
        self._setp = var

    @property
    def watchdog(self):
        return self._watchdog

    @watchdog.setter
    def watchdog(self, var):
        self._watchdog = var

    def get_current_joint_angles(self) -> np.ndarray:
        """Retrieves current joint angles from the robot.

        This is a convenience method, which reformats the joint angles from a
        dict into a numpy array for easier processing.

        .. note::
            If more data is required, please use the :class:`Ur5e.get_data()` method.

        Returns:
            :class:`numpy.ndarray`: Actual joint angles from q0 -> q5

        """

        data = self.get_data(np.array([ROBOT_OUTPUT_DATA.actual_q]))

        out = np.array([])
        for key, value in data.items():
            out = np.append(out, np.array([value]))

        return out

    def get_data(
        self, requested_data: np.array, samples: int = 1, frequency: int = 125
    ) -> dict:
        """Get data from the robot.

        Args:
            requested_data (:class:`numpy.ndarray`): The data that should be returned.
            samples (int): The number of data that should be returned of each type.
            frequency (int): The frequency with which to communicate with the robot.

        Raises:
            AssertionError: The frquency and number of requested samples arguments must be greater than zero!

        """

        assert samples > 0, """Number of requested samples must be greater than 0!"""
        assert frequency > 0, """Frequency must be greater than 0!"""

        # sanity check
        for i in requested_data:
            if type(i) is not types.ROBOT_OUTPUT_DATA:
                raise RuntimeError(
                    "Requested robot output data should be of type ROBOT_OUTPUT_DATA!"
                )

        # convert requested data from enum to names
        requested_names = np.array([])
        for i in requested_data:
            requested_names = np.append(requested_names, i.name)

        # initialize output array
        out = np.array([])

        # actually get the data
        for i in range(samples):
            out = np.append(out, self._get_single_data())

        # sanity check
        assert out.shape[0] == samples

        # assemble output dict
        assert self._output_names is not None and self._output_types is not None

        writer = DictWriter(self._output_names, self._output_types)
        writer.writeheader()
        for i in out:
            writer.writerow(i)

        # -> need to modify the requested keys in order to account for 6 dofs.
        dof_adjusted_reqested_keys = np.array([])
        for req_n in requested_names:
            for head_n in writer.header_names:
                if req_n in head_n:  # check if we have a partial match
                    # print("Partial match! req_n: ", req_n)

                    dof_adjusted_reqested_keys = np.append(
                        dof_adjusted_reqested_keys, np.array([head_n])
                    )

        # print("dof_ad: ", dof_adjusted_reqested_keys)

        # prune the dict. Only keep fields that are actually requested
        outdata = dict()  # stores pruned version
        for key, value in writer.outdata.items():  # iterate through all keys
            if key in dof_adjusted_reqested_keys:
                outdata[key] = value

        return outdata

    def _get_single_data(self):
        """Get a single data package from the robot.

        .. note::
            Always gets all data that is defined in the connection coniguration xml file.

        Returns:
            Serialized output data of singel measurement.

        """

        try:
            state = self._con.receive()

        except KeyboardInterrupt:
            RuntimeError("Keyboard interrupt!")

        except rtde.RTDEException:
            self._con.disconnect()
            RuntimeError("An RTDE Exception has occurred!")

        return state
