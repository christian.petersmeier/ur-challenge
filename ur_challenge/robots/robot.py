"""Defines a generic robot."""

from ur_challenge.types.types import ROBOT_OUTPUT_DATA
import rtde
from abc import ABC
import numpy as np


class Robot(ABC):
    """A generic robot type."""

    def __init__(
        self, ip_address: str, port: int, frequency: int = 125, name: str = ""
    ) -> None:
        # robot details
        self.name = name

        # connection details
        self.ip_address = ip_address
        self.port = port
        self.frequency = frequency

        # RTDE connection
        self._con = None
        self._connect(ip_address, port)

    def get_data(
        self, requested_data: np.array, samples: int = 1, frequency: int = 125
    ):
        raise NotImplementedError()

    def _connect(self, ip_address: str, port: int):
        """Connect to a real or simulated robot via TCP / IP.

        The function uses the RTDE protocol.

        Args:
            ip_address (str): The IP Address of the real or
              simulated robot controller.
            port (int): The port over which to connect to the robot.

        """
        self._con = rtde.RTDE(hostname=ip_address, port=port)
        self._con.connect()
        if self._con.is_connected():
            print("Robot connected successfuly!")
        else:
            RuntimeError("Robot connection could not be established!")

    def disconnect(self):
        """Disconnect from the robot."""

        self._con.send_pause()
        self._con.disconnect()

    @property
    def con(self):
        """RTDE connection to the robot."""
        if self._con is not None and self._con.is_connected():
            return self._con
        else:
            RuntimeError("No RTDE connection has been established yet!")
