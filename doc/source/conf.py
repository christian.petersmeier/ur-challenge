# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "UR-Challenge"
copyright = "2023, Christian Petersmeier"
author = "Christian Petersmeier"

# for type hints in the documentation
import numpy

# import sys, os
# external_module_path = os.path.abspath('./../../external/RTDE_Python_Client_Library/rtde')
# sys.path.insert(0, external_module_path)

# print("path: ", sys.path)
# print("python version: ", sys.version)

# from RTDE_Python_Client_Library.rtde import serialize

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ["sphinx_rtd_theme", "sphinx.ext.autodoc", "sphinx.ext.napoleon"]

templates_path = ["_templates"]
exclude_patterns = []

add_module_names = False

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]
