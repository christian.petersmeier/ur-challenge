.. UR-Challenge documentation master file, created by
   sphinx-quickstart on Fri Dec 15 13:34:18 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to UR-Challenge's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api/api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
