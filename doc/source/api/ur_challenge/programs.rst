.. UR-Challenge documentation master file, created by
   sphinx-quickstart on Fri Dec 15 13:34:18 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Programs
========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: ur_challenge.programs.program
    :members:
    :undoc-members:
    :private-members:

.. automodule:: ur_challenge.programs.basic_two_wp_cyle_const_stream
    :members:
    :undoc-members:
    :private-members:

.. automodule:: ur_challenge.programs.basic_two_wp_cyle
    :members:
    :undoc-members:
    :private-members:
