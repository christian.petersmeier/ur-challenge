.. UR-Challenge documentation master file, created by
   sphinx-quickstart on Fri Dec 15 13:34:18 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Robots
======

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: ur_challenge.robots.robot
    :members:
    :undoc-members:
    :private-members:

.. automodule:: ur_challenge.robots.UR5e
    :members:
    :undoc-members:
    :private-members:
