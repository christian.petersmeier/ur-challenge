import numpy as np
from ur_challenge.robots.UR5e import UR5e
from ur_challenge.types.types import ROBOT_OUTPUT_DATA
from ur_challenge.programs.basic_two_wp_cyle_const_stream import (
    BasicTwoWPCycleConstStream,
)

###########################
# Connect to robot
###########################
ip_address = "192.168.178.133"
port = 30004
robot = UR5e(ip_address, port)

###########################
# Get information
###########################

# Define which information is needed
data_request = np.array(
    [
        ROBOT_OUTPUT_DATA.timestamp,
        ROBOT_OUTPUT_DATA.actual_q,
        ROBOT_OUTPUT_DATA.actual_current,
        ROBOT_OUTPUT_DATA.joint_temperatures,
    ]
)

# Actually get the data
data_response = robot.get_data(data_request, samples=10)

###########################
# Run Cycle program
###########################

# Execution guard
run = True

# Initialize the program
program = BasicTwoWPCycleConstStream(robot)

# Run the program
program.enter()
while run:
    program.step()
program.exit()
