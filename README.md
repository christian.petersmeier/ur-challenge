# UR-Challenge

This repository holds the development for the solution of the UR-Challenge by Christian Petersmeier.

## Environment Setup

In order to get started, the project needs to be set-up first. Clone the repository and initialize the
submodules:

```
git clone https://gitlab.ub.uni-bielefeld.de/christian.petersmeier/ur-challenge.git
cd ur-challenge
git submodule init
git submodule update
```

We need to make sure that the python environment knows of our `ur_challenge` package, as well as of the
`rtde` packages inside the submodule. Please add them to your path accordingly. On windows, the path entries
might look like this:

```
F:\VIRTUAL_MACHINES\ur-challenge\
F:\VIRTUAL_MACHINES\ur-challenge\external\RTDE_Python_Client_Library\rtde
```

## Getting Started:

### Executing the program

In order to run the program, put the robot controller in local mode and load up the
`rtde_control_loop_const_stream.urp` program. In the `scripts/main.py` file, adjust
the IP Address to match the robot and execute the script:

```
python main.py
```

Next, execute the control loop script on the UR hardware controller. The robot should
move between two waypoints.

### Building the documentation

Please make sure you have sphinx installed. Next, navigate to the `doc/` folder and
trigger the documentation generation

```
make html
```

You can find the documentation under `doc/build/html/index.html`

## Repository Contents

The following overview lists the contents of this repository:

```
config/         Contains configuration files for rtde communication.
doc/            Contains project documentation.
external/       External libraries as git submodules.
scripts/        Executable scripts are placed here.
ur_challenge/   Python package that implements the tasks of the ur-challenge.
urp/            Robot programs.
```

## UR-Challenge Task

### Task Description

The following list holds a summary of the provided task PDF:

Develop a Python program to remote control the robot:
  - Implement automated motion between **two** waypoints (w0, w1) in joint or taskspace coordinates.
  - Read out current joint config and only drive towards w1 if w0 has been reached and vice versa.
  - Read out all or a subset of robot data. E.g. joint currents, temperatures, positions, etc ..

Constraints on the program structure:
  - Object-Oriented Program Paradigm
  - User-Friendly API: encapsulation of sending and receiving TCP/IP data via easy-to-understand functions.

### Resources

The following resources have been provided in order to aid development:

*General:*

[PolyScope Simulation Environment](https://www.universal-robots.com/products/polyscope/)

*API Documentation:*

[Teal-Time Data Exchange (RTDE) Guide](https://www.universal-robots.com/articles/ur/interface-communication/real-time-data-exchange-rtde-guide/)

[Remote control via TCP/IP](https://www.universal-robots.com/articles/ur/interface-communication/remote-control-via-tcpip/)
